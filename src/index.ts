interface Developer{
    id :number
    name :string
    isReady :string | boolean
    uses? :Developer[]
}


const dev1 :Developer = {
    id: 1,
    name: 'dev1',
    isReady: 'YES'
  };
  
  const devices :Developer[]= [
    dev1,
    {
      id: 2,
      name: 'dev2',
      isReady: 'NO'
    },
    {
      id: 3,
      name: 'dev3',
      isReady: true,
      uses: [ dev1 ]
    }
  ];
  